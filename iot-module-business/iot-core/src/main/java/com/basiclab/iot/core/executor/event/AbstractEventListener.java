package com.basiclab.iot.core.executor.event;


import com.basiclab.iot.core.executor.event.common.IEvent;
import com.basiclab.iot.core.executor.event.common.IEventListener;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by jiangwenping on 17/1/6.
 */
public abstract class AbstractEventListener implements IEventListener {

    private final Set<EventType> set;

    public AbstractEventListener() {
        this.set = new CopyOnWriteArraySet<EventType>();
        initEventType();
    }

    public abstract void initEventType();

    @Override
    public void register(EventType eventType) {
        this.set.add(eventType);
    }

    public void unRegister(EventType eventType) {
        this.set.remove(eventType);
    }

    @Override
    public boolean containEventType(EventType eventType) {
        return set.contains(eventType);
    }

    @Override
    public void fireEvent(IEvent event) {
        event.call();
    }

    public Set<EventType> getSet() {
        return set;
    }

}
