package com.basiclab.iot.core.logic.player;

import com.basiclab.iot.core.service.net.tcp.session.NettyTcpNetMessageSender;

/**
 * Created by jiangwenping on 17/2/20.
 */
public interface IPlayer {
    public long getPlayerId();
    public int getPlayerUdpTocken();
    public NettyTcpNetMessageSender getNettyTcpNetMessageSender();
}
