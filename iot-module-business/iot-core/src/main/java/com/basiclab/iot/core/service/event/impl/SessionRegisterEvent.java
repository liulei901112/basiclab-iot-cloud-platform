package com.basiclab.iot.core.service.event.impl;

import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.SingleEvent;
import com.basiclab.iot.core.service.event.SingleEventConstants;

/**
 * Created by jiangwenping on 2017/5/22.
 * 网络链接事件建立
 */
public class SessionRegisterEvent extends SingleEvent<Long> {

    public SessionRegisterEvent(Long eventId, long shardingId, EventParam... parms) {
        super(SingleEventConstants.sessionRegister, eventId, shardingId, parms);
    }

}
