package com.basiclab.iot.core.executor.event.impl.event;

import com.basiclab.iot.core.executor.event.CycleEvent;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.EventType;

import java.io.Serializable;

/**
 * Created by jiangwenping on 17/2/21.
 * updateService使用
 */
public class FinishedEvent<ID extends Serializable> extends CycleEvent {

    public FinishedEvent(EventType eventType, ID eventId, EventParam... parms){
        super(eventType, eventId, parms);
    }

    @Override
    public void call() {
//        EventParam[] eventParams = getParams();
//        System.out.println(eventParams[0].getT() + "float"+ eventParams[1].getT());
    }
}
