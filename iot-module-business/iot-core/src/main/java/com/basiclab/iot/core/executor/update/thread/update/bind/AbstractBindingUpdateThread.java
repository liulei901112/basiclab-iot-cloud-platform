package com.basiclab.iot.core.executor.update.thread.update.bind;

import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.event.EventBus;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.impl.event.UpdateEvent;
import com.basiclab.iot.core.executor.update.cache.UpdateEventCacheService;
import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.thread.dispatch.DispatchThread;
import com.basiclab.iot.core.executor.update.thread.update.UpdateThread;

/**
 * Created by jiangwenping on 17/1/9.
 * 带预置锁的执行器
 */
public abstract class AbstractBindingUpdateThread extends UpdateThread {

    private final DispatchThread dispatchThread;

    public AbstractBindingUpdateThread(DispatchThread dispatchThread, EventBus eventBus) {
        super(eventBus);
        this.dispatchThread = dispatchThread;
    }

    public void run() {
        if(getiUpdate() != null) {
            IUpdate excutorUpdate = getiUpdate();
            excutorUpdate.update();
            setiUpdate(null);

            //事件总线增加更新完成通知
            EventParam<IUpdate> params = new EventParam<IUpdate>(excutorUpdate);
            UpdateEvent updateEvent = UpdateEventCacheService.createUpdateEvent();
            updateEvent.setEventType(Constants.EventTypeConstans.updateEventType);
            updateEvent.setId(excutorUpdate.getUpdateId());
            updateEvent.setParams(params);
//            UpdateEvent event = new UpdateEvent(Constants.EventTypeConstans.updateEventType, excutorUpdate.getUpdateId(), params);
            updateEvent.setUpdateAliveFlag(getiUpdate().isActive());
            getEventBus().addEvent(updateEvent);
        }
    }


    public DispatchThread getDispatchThread() {
        return dispatchThread;
    }
}
