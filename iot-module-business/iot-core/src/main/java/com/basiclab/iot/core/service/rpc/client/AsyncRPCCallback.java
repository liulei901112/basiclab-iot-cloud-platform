package com.basiclab.iot.core.service.rpc.client;

/**
 */
public interface AsyncRPCCallback {

    void success(Object result);

    void fail(Exception e);

}
