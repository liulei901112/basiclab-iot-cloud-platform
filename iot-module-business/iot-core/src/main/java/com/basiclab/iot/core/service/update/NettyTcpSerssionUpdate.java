package com.basiclab.iot.core.service.update;

import com.basic.common.network.constant.Loggers;
import com.basiclab.iot.core.executor.update.entity.AbstractUpdate;
import com.basiclab.iot.core.service.net.tcp.session.NettyTcpSession;
import com.basiclab.iot.core.service.net.tcp.session.TcpNetState;

/**
 * Created by jiangwenping on 17/2/14.
 */
public class NettyTcpSerssionUpdate extends AbstractUpdate<Long> {

    private static final long serialVersionUID = -4490585465336581804L;
    private final NettyTcpSession nettyTcpSession;

    public NettyTcpSerssionUpdate(NettyTcpSession nettyTcpSession) {
        this.nettyTcpSession = nettyTcpSession;
    }

    @Override
    public void update() {
        nettyTcpSession.update();
        updateAlive();
        if(Loggers.sessionLogger.isDebugEnabled()){
            Loggers.sessionLogger.debug("update session update id " + getUpdateId());
        }
    }

    @Override
    public Long getUpdateId() {
        return nettyTcpSession.getSessionId();
    }

    public void updateAlive(){
        if(nettyTcpSession.getTcpNetStateUpdate().state == TcpNetState.DESTROY){
            setActive(false);
        }
    }
}
