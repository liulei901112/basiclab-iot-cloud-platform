package com.basiclab.iot.core.service.rpc.client;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basic.common.network.config.GameServerDiffConfig;
import com.basic.common.network.constant.Loggers;
import com.basic.common.network.constant.ServiceName;
import com.basic.common.network.enums.BOEnum;
import com.basiclab.iot.core.service.IService;
import com.basiclab.iot.core.service.config.GameServerConfigService;
import com.basiclab.iot.core.service.rpc.client.impl.DbRpcConnnectMananger;
import com.basiclab.iot.core.service.rpc.client.impl.GameRpcConnecetMananger;
import com.basiclab.iot.core.service.rpc.client.impl.WorldRpcConnectManager;
import com.basiclab.iot.core.service.rpc.server.SdServer;
import com.basiclab.iot.core.service.rpc.server.zookeeper.ZooKeeperNodeBoEnum;
import com.basiclab.iot.core.service.rpc.server.zookeeper.ZooKeeperNodeInfo;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jwp on 2017/3/8.
 * rpc的服务发现
 */
@Service
public class RpcClientConnectService implements IService {

    private static final Logger LOGGER = Loggers.rpcLogger;

    protected Object lock = new Object();


    @Autowired
    private WorldRpcConnectManager worldRpcConnectManager;

    @Autowired
    private GameRpcConnecetMananger gameRpcConnecetMananger;

    @Autowired
    private DbRpcConnnectMananger dbRpcConnnectMananger;

    public void initWorldConnectedServer(List<SdServer> sdServerList) throws Exception {
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        worldRpcConnectManager.initServers(sdServerList);
    }

    public void initGameConnectedServer(List<SdServer> sdServerList) throws Exception {
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        gameRpcConnecetMananger.initServers(sdServerList);
    }

    public void initDbConnectServer(List<SdServer> sdServerList) throws Exception {
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        dbRpcConnnectMananger.initServers(sdServerList);
    }

    @Override
    public String getId() {
        return ServiceName.RpcServiceDiscovery;
    }

    @Override
    public void startup() throws Exception {
        worldRpcConnectManager.initManager();
        gameRpcConnecetMananger.initManager();
        dbRpcConnnectMananger.initManager();
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        GameServerDiffConfig gameServerDiffConfig = gameServerConfigService.getGameServerDiffConfig();
        if(!gameServerDiffConfig.isZookeeperFlag()) {
            init();
        }
    }

    @Override
    public void shutdown() throws Exception {
        worldRpcConnectManager.stop();
        gameRpcConnecetMananger.stop();
        dbRpcConnnectMananger.stop();
    }

    public void init() throws Exception {
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        initWorldConnectedServer(gameServerConfigService.getRpcServerRegisterConfig().getSdWorldServers());
        initGameConnectedServer(gameServerConfigService.getRpcServerRegisterConfig().getSdGameServers());
        initDbConnectServer(gameServerConfigService.getRpcServerRegisterConfig().getSdDbServers());
    }


    public AbstractRpcConnectManager getRpcConnectMannger(BOEnum boEnum){
        AbstractRpcConnectManager abstractRpcConnectManager = worldRpcConnectManager;
        if(boEnum == BOEnum.GAME){
        }else if (boEnum == BOEnum.DB){
        }
        return worldRpcConnectManager;
    }

    public AbstractRpcConnectManager getRpcConnectMannger(ZooKeeperNodeBoEnum zooKeeperNodeBoEnu){
        AbstractRpcConnectManager abstractRpcConnectManager = worldRpcConnectManager;
        if(zooKeeperNodeBoEnu == ZooKeeperNodeBoEnum.GAME){
        }else if (zooKeeperNodeBoEnu == ZooKeeperNodeBoEnum.DB){
        }
        return worldRpcConnectManager;
    }

    public void notifyConnect(ZooKeeperNodeBoEnum zooKeeperNodeBoEnum, List<ZooKeeperNodeInfo> zooKeeperNodeInfoList) throws InterruptedException {
       getRpcConnectMannger(zooKeeperNodeBoEnum).initZookeeperRpcServers(zooKeeperNodeInfoList);
    }


}

