//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;


import com.basiclab.iot.core.expression.Expression;

public class ParamExpression implements Expression {
    private static final long serialVersionUID = -1691851142296564067L;

    public ParamExpression() {
    }

    @Override
    public long getValue(long key) {
        return key;
    }
}
