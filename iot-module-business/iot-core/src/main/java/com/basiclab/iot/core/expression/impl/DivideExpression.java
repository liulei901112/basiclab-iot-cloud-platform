//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;

public class DivideExpression extends BinaryOperationExpression {
    private static final long serialVersionUID = -6210937060757444787L;

    public DivideExpression() {
    }

    @Override
    public long getValue(long key) {
        return this.left.getValue(key) / this.right.getValue(key);
    }

    @Override
    public int getPriority() {
        return 2;
    }
}
