package com.basiclab.iot.core.executor.event.factory;

import com.basiclab.iot.core.executor.event.CycleEvent;
import com.lmax.disruptor.EventFactory;

/**
 * Created by jiangwenping on 17/4/24.
 */
public class CycleDisruptorEventFactory implements EventFactory<CycleEvent> {

    @Override
    public CycleEvent newInstance() {
        return new CycleEvent();
    }

}
